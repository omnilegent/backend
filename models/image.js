"use strict";
module.exports = (sequelize, DataTypes) => {
	const Image = sequelize.define(
		"Image",
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				references: {
					model: "Conversations",
					key: "id"
				}
			},
			userId: {
				type: DataTypes.INTEGER,
				allowNull: false,
				references: {
					model: "Users",
					key: "id"
				}
			},
			imageName: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true
			}
		},
		{}
	);
	Image.associate = function(models) {
		// associations can be defined here
	};
	return Image;
};
