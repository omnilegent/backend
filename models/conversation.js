"use strict";
module.exports = (sequelize, DataTypes) => {
	const Conversation = sequelize.define(
		"Conversation",
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			userId: {
				type: DataTypes.INTEGER,
				allowNull: false,
				references: {
					model: "Users",
					key: "id"
				}
			},
			title: {
				type: DataTypes.STRING
			},
			description: {
				type: DataTypes.STRING
			},
			text: {
				type: DataTypes.TEXT
			},
			tags: {
				type: DataTypes.ARRAY(DataTypes.STRING)
			}
		},
		{}
	);
	Conversation.associate = function(models) {
		// associations can be defined here
	};
	return Conversation;
};
