const Router = require("koa-router");
const bcrypt = require("bcryptjs");
const shortId = require("shortid");

const { User } = require("../models/index");

const signup = new Router({
	prefix: "/api/signup"
});
const salt = bcrypt.genSaltSync(10);

signup.get("/", async ctx => (ctx.body = "GET /signup"));
signup.post("/", async ctx => {
	try {
		ctx.body = "POST /Signup";
		const { email, password } = ctx.request.body;
		const user = await User.findOne({
			where: {
				email
			}
		});
		if (user) {
			ctx.status = 422;
			ctx.body = "This email is already taken";
		} else {
			try {
				const hash = bcrypt.hashSync(password, salt);
				User.create({
					email,
					password: hash,
					folderName: shortId.generate()
				}).then(user => {
					ctx.status = 200;
					ctx.body = "User successfully created";
				});
			} catch (e) {
				ctx.status = 500;
				ctx.body =
					"The server encountered an error while trying to create a new user";
			}
		}
	} catch (e) {
		console.log("An unexpected error occured: ", e);
	}
});

module.exports = signup;
