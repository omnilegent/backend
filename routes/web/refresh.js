const Router = require("koa-router");
const jwt = require("jsonwebtoken");
const bodyParser = require("koa-bodyparser");

const refresh = new Router({
	prefix: "/api/web/refresh"
});

const { generateAccessToken, verifyToken } = require("../../auth/config");

refresh.get("/", async ctx => (ctx.body = "GET /refresh"));
refresh.post("/", async ctx => {
	const refreshToken = ctx.cookies.get("refresh-token");

	try {
		// verify access-token from cookies
		const refreshTokenData = verifyToken(refreshToken);
		console.log("\n\nrefreshTokenData", refreshTokenData);
		ctx.status = 200;
		ctx.body =
			"Your refresh-token was fine so I created a new access token for you";
	} catch (err) {
		// Expired - create and return new
		if (err.name === "TokenExpiredError") {
			// read refresh-token from cookies
			ctx.status = 200;
			const refreshTokenData = verifyToken(refreshToken);
			const accessToken = generateAccessToken(refreshTokenData.id);
			ctx.cookies.set("access-token", accessToken.token);
			ctx.body = "Access-token refreshed";
			console.log(
				"Access-token was expired but refresh-token was not so I created a new access-token"
			);
		} else {
			console.log("An unexpected error occured while trying to LOGIIIN", err);
		}
	}
});

module.exports = refresh;
