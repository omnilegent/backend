const Router = require("koa-router");
const bcrypt = require("bcryptjs");

const login = new Router({
	prefix: "/api/web/login"
});

const { User } = require("../../models/index");
const {
	generateAccessToken,
	generateRefreshToken
} = require("../../auth/config");

login.get("/", async ctx => (ctx.body = "GET /login"));
login.post("/", async ctx => {
	ctx.body = "POST /login";
	try {
		console.log("/web/login ctx.request.body", ctx.request.body);
		const { email, password } = ctx.request.body;
		const user = await User.findOne({
			where: {
				email
			}
		});
		if (user === null)
			throw new Error("There is no registered user with that email address");
		const { password: hash } = user.get();
		if (user && bcrypt.compareSync(password, hash)) {
			const accessToken = generateAccessToken(user.get().id);
			const refreshToken = generateRefreshToken(user.get().id);
			ctx.status = 200;

			// TODO: set a cookie expiration date ?
			ctx.cookies.set("access-token", accessToken.token, {
				httpOnly: false
			});
			ctx.cookies.set("refresh-token", refreshToken.token, {
				httpOnly: false
			});
			ctx.body = "Login successful";
		} else if (user && !bcrypt.compareSync(password, hash)) {
			ctx.status = 401;
			ctx.body = "Incorrect password";
		} else {
		}
	} catch (err) {
		if (err.message === "There is no registered user with that email address") {
			ctx.status = 404;
			ctx.body = "No matching user found";
		} else console.log("An unexpected error occured", err);
	}
});

module.exports = login;
