const Router = require("koa-router");
const { authenticate } = require("../../auth/config");

const router = new Router({
	prefix: "/api/web"
});

const { User, Conversation } = require("../../models/index");

router.get("/view/:id", authenticate, async ctx => {
	try {
		const conversationId = ctx.params.id;
		const conversation = await Conversation.findOne({
			where: {
				id: conversationId
			}
		});
		if (conversation) {
			ctx.status = 200;
			ctx.body = conversation;
		} else {
			ctx.status = 404;
			ctx.body = "No such conversation found";
		}
	} catch (err) {
		console.log("\n weird eror at /api/web/view:id");
	}
});

router.post("/edit", authenticate, async ctx => {
	const { id, title, description, text, tags } = ctx.request.body;
	try {
		const conversation = await Conversation.update(
			{
				title,
				description,
				text
				// tags
			},
			{
				where: {
					id
				}
			}
		);
		if (conversation) {
			ctx.status = 200;
			ctx.body = "Conversation successfully updated";
		} else {
			ctx.status = 404;
			ctx.body = "No such conversation found";
		}
	} catch (err) {
		console.log("error in post /api/web/edit", err);
	}
});

router.put("/create", authenticate, async ctx => {
	console.log("ctx.request.body is", ctx.request.body);
	try {
		const { id } = ctx.accessTokenData;
		const { title, description, text, tags } = ctx.request.body;
		console.log("title", title);
		console.log("description", description);
		console.log("text", text);
		if (!title || !description || !text) {
			ctx.status = 400;
			ctx.body =
				"Every conversation needs to have a title, description and actual text";
		} else {
			const conversation = await Conversation.create({
				userId: id,
				title,
				description,
				text,
				tags: ["test", "test2"]
			});
			if (conversation) {
				ctx.status = 200;
				ctx.body = "Successfully created a conversation";
			} else {
				ctx.status = 500;
				ctx.body = "Error while trying to save your conversation";
			}
		}
	} catch (err) {
		console.log("Error in  PUT /api/web/create", err);
	}
});

router.delete("/delete/:id", authenticate, async ctx => {
	const id = ctx.params.id;
	console.log("\n\n/delete/id is", Object.keys(ctx.params.id));
	// Object.keys(ctx.params)
	try {
		const conversation = await Conversation.destroy({
			where: {
				id
			}
		});
		if (conversation) {
			ctx.status = 200;
			ctx.body = "Conversation successfully deleted";
		} else {
			ctx.status = 500;
			ctx.body = "Unable to delete conversation O.O";
		}
	} catch (err) {
		console.log("error in post /api/web/edit", err);
	}
});

module.exports = router;
