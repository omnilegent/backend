const Router = require("koa-router");

const { User, Conversation } = require("../../models/index");
const { authenticate, getAccessTokenData } = require("../../auth/config");

const router = new Router({
	prefix: "/api/web"
});

router.get("/", authenticate, async ctx => {
	try {
		const { id } = ctx.accessTokenData;
		// ctx.body = "GET /api/web";
		// Get conversations
		const conversations = await Conversation.findAll({
			where: {
				userId: id
			}
		});
		ctx.status = 200;
		ctx.body = conversations;
	} catch (err) {
		ctx.status = 500;
		console.log("Unexpected error at GET /api/web", err);
	}
});
router.post("/", async ctx => (ctx.body = "POST /api/web"));

module.exports = router;
